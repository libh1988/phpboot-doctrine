<?php

use phpboot\doctrine\Console\Console;


$current_dir = getcwd();

$loader = require_once "vendor/autoload.php";

$config_path = $current_dir . "/console.php";

if (!is_file($config_path)) {
    echo "No console.php file was found";
    exit;
}
$config = require_once $config_path;

$key = isset($config['key']) ? $config['key'] : "default";
$loader->setPsr4("", $config['paths']);
Console::run($config, $key);
