<?php


return [
    'isDevMode' => true,
    'proxyDir' => null,
    'cache' => null,
    'useSimpleAnnotationReader' => false,
    'driver' => 'pdo_mysql',
    'type' => 'mysql',
    'host' => '127.0.0.1',
    'dbname' => 'test_db',
    'user' => 'root',
    'password' => 'root',
    'port' => 3306,
    'prefix' => '',
    'paths' => [__DIR__ . "/schema"],
];

// doctrine phpboot:update  将更新文件信息生成即将同步到数据库的SQL
// doctrine phpboot:update -f  将更新文件信息生成同步到数据库的SQL并执行

// doctrine phpboot:mapping  将数据库结构生成文件
// doctrine phpboot:mapping --fliter=User 将数据库结构生成文件,带过滤功能
// doctrine phpboot:mapping --fliter=^User$ 将数据库结构生成文件,带过正则过滤功能
