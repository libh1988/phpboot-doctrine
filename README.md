整合和简化doctrine的配置和使用

执行安装


```
composer install
```

配置好后，可以执行doctrine所有的命令，具体用法可以去doctrine查看

```
 php doctrine 具体命令
```
在此基础上，添加了两个命令,其实也是doctrine的命令，主要是简化doctrine的命令操作,具体用法如下


```
php doctrine phpboot:update  //将更新文件信息生成即将同步到数据库的SQL
php doctrine phpboot:update -f //将更新文件信息生成同步到数据库的SQL并执行

php doctrine phpboot:mapping  //将数据库结构生成文件
php doctrine phpboot:mapping --fliter=User //将数据库结构生成文件,带过滤功能
php doctrine phpboot:mapping --fliter=^User$ //将数据库结构生成文件,带过正则过滤功能
```

**建议将 phpboot-doctrine 当成单独的项目，并将项目的目录加入到环境变量中**

然后将 console.php 文件复制到您的开发项目位置，将里面的配置修改成您的项目配置

这时您只要cd到您console.php所在的位置，就可以像下面的方式执行了

```
doctrine phpboot:update  //将更新文件信息生成即将同步到数据库的SQL
doctrine phpboot:update -f //将更新文件信息生成同步到数据库的SQL并执行

doctrine phpboot:mapping  //将数据库结构生成文件
doctrine phpboot:mapping --fliter=User //将数据库结构生成文件,带过滤功能
doctrine phpboot:mapping --fliter=^User$ //将数据库结构生成文件,带过正则过滤功能
```
