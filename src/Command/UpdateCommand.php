<?php


namespace phpboot\doctrine\Command;


use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateCommand extends \Doctrine\ORM\Tools\Console\Command\SchemaTool\UpdateCommand
{

    protected function configure()
    {
        parent::configure();
        $this->setName("phpboot:update");
    }
    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $input->setOption("dump-sql",true);
        return parent::execute($input, $output);
    }
}
