<?php


namespace phpboot\doctrine\Command;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Console\Command\ConvertMappingCommand;
use Doctrine\ORM\Tools\SchemaTool;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\ArrayInput;

class MappingCommand extends ConvertMappingCommand
{
    protected function configure()
    {
        $this->setName('phpboot:mapping')
            ->setDescription('Convert mapping information between supported formats')
            ->addArgument('to-type', null, 'The mapping type to be converted.')
            ->addArgument('dest-path', null, 'The path to generate your entities classes.')
            ->addOption('filter', null, InputOption::VALUE_REQUIRED | InputOption::VALUE_IS_ARRAY, 'A string pattern used to match entities that should be processed.')
            ->addOption('force', 'f', InputOption::VALUE_NONE, 'Force to overwrite existing mapping files.')
            ->addOption('from-database', null, null, 'Whether or not to convert mapping information from existing database.')
            ->addOption('extend', null, InputOption::VALUE_OPTIONAL, 'Defines a base class to be extended by generated entity classes.')
            ->addOption('num-spaces', null, InputOption::VALUE_OPTIONAL, 'Defines the number of indentation spaces', 4)
            ->addOption('namespace', null, InputOption::VALUE_OPTIONAL, 'Defines a namespace for the generated entity classes, if converted from database.')
            ->setHelp(<<<EOT
Convert mapping information between supported formats.

This is an execute <info>one-time</info> command. It should not be necessary for
you to call this method multiple times, especially when using the <comment>--from-database</comment>
flag.

Converting an existing database schema into mapping files only solves about 70-80%
of the necessary mapping information. Additionally the detection from an existing
database cannot detect inverse associations, inheritance types,
entities with foreign keys as primary keys and many of the
semantical operations on associations such as cascade.

<comment>Hint:</comment> There is no need to convert YAML or XML mapping files to annotations
every time you make changes. All mapping drivers are first class citizens
in Doctrine 2 and can be used as runtime mapping for the ORM.

<comment>Hint:</comment> If you have a database with tables that should not be managed
by the ORM, you can use a DBAL functionality to filter the tables and sequences down
on a global level:

    \$config->setFilterSchemaAssetsExpression(\$regexp);
EOT
            );
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var  $em EntityManagerInterface */
        $em = $this->getHelper("em")->getEntityManager();
        $config = $em->getConnection()->getParams();
        $input->setArgument("dest-path", current($config['paths']));
        $input->setArgument("to-type", 'annotation');
        $input->setOption("from-database", true);
        $input->setOption("force", true);

        return parent::execute($input, $output);
    }
}
