<?php


namespace phpboot\doctrine\Console;


use phpboot\doctrine\EntityManager;

class Console
{
    public static function run($db_config, $key = "default")
    {
        $helperSet = ConsoleRunner::createHelperSet(EntityManager::getManager($db_config, $key));

        //$helperSet->set("");
        ConsoleRunner::run($helperSet, []);
    }

}
